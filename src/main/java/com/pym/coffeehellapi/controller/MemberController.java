package com.pym.coffeehellapi.controller;

import com.pym.coffeehellapi.model.member.MemberCreateRequest;
import com.pym.coffeehellapi.model.member.MemberInfoChangeRequest;
import com.pym.coffeehellapi.model.member.MemberItem;
import com.pym.coffeehellapi.model.member.MemberResponse;
import com.pym.coffeehellapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")
    public String setMember(@RequestBody MemberCreateRequest request){
        memberService.setMember(request);

        return "OK";
    }

    @GetMapping("/list")
    public List<MemberItem> getMembers(){
        return memberService.getMembers();
    }

    @GetMapping("/detail/{memberId}")
    public MemberResponse getMember(@PathVariable long memberId){
        return memberService.getMember(memberId);
    }

    @PutMapping("/info/member-id/{memberId}")
    public String putMemberInfo(@PathVariable long memberId, @RequestBody MemberInfoChangeRequest request){
        memberService.putMemberInfo(memberId, request);

        return "수정";
    }
}
