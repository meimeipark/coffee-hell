package com.pym.coffeehellapi.controller;

import com.pym.coffeehellapi.entity.Member;
import com.pym.coffeehellapi.model.menu.MenuChangeRequest;
import com.pym.coffeehellapi.model.menu.MenuCreateRequest;
import com.pym.coffeehellapi.model.menu.MenuItem;
import com.pym.coffeehellapi.model.menu.MenuResponse;
import com.pym.coffeehellapi.service.MemberService;
import com.pym.coffeehellapi.service.MenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/menu")
public class MenuController {
    private final MenuService menuService;
    private final MemberService memberService;

    @PostMapping("/add/member-id/{memberId}")
    public String setMenu (@PathVariable long memberId,@RequestBody MenuCreateRequest request){
        Member member = memberService.getData(memberId);
        menuService.setMenu(member, request);

        return "메뉴 기록";
    }

    @GetMapping("/list")
    public List<MenuItem> getMenus(){
        return menuService.getMenus();
    }

    @GetMapping("/detail/{menuId}")
    public MenuResponse getMenu(@PathVariable long menuId){
        return menuService.getMenu(menuId);
    }

    @PutMapping("/menu-id/{menuId}")
    public String putMenu (@PathVariable long menuId, @RequestBody MenuChangeRequest request){
        menuService.putMenu(menuId, request);

        return "수정완료";
    }

    @DeleteMapping("/menu-id/{menuId}")
    public String delMenu(@PathVariable long menuId){
        menuService.delMenu(menuId);

        return "삭제";
    }
}
