package com.pym.coffeehellapi.repository;

import com.pym.coffeehellapi.entity.Menu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MenuRepository extends JpaRepository<Menu, Long> {
}
