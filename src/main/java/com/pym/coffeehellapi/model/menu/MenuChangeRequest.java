package com.pym.coffeehellapi.model.menu;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MenuChangeRequest {
    private Double totalPrice;
    private LocalDate hellDate;
    private String menuEtc;
}
