package com.pym.coffeehellapi.model.menu;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MenuCreateRequest {
    private Double totalPrice;
    private LocalDate hellDate;
    private String menuEtc;
}
