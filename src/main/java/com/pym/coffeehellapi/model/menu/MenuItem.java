package com.pym.coffeehellapi.model.menu;

import com.pym.coffeehellapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class MenuItem {
    private Long id;
    private String memberName;
    private Double totalPrice;
    private LocalDate hellDate;
}
