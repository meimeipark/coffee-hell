package com.pym.coffeehellapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberResponse {
    private Long id;
    private String name;
    private String phoneNumber;
    private String etcMemo;
}
