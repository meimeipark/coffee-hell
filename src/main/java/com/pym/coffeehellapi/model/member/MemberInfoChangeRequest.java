package com.pym.coffeehellapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberInfoChangeRequest {
    private String name;
    private String phoneNumber;
    private String etcMemo;
}
