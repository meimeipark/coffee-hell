package com.pym.coffeehellapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "Coffee Hell Api App",
                description = "Coffee Hell app api명세",
                version = "v1")
)
@RequiredArgsConstructor
@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi v1OpenApi(){
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("죽음의 커피 내기 API v1")
                .pathsToMatch(paths)
                .build();
    }
}
