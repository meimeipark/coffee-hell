package com.pym.coffeehellapi.service;

import com.pym.coffeehellapi.entity.Member;
import com.pym.coffeehellapi.entity.Menu;
import com.pym.coffeehellapi.model.menu.MenuChangeRequest;
import com.pym.coffeehellapi.model.menu.MenuCreateRequest;
import com.pym.coffeehellapi.model.menu.MenuItem;
import com.pym.coffeehellapi.model.menu.MenuResponse;
import com.pym.coffeehellapi.repository.MenuRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MenuService {
    private final MenuRepository menuRepository;

    public void setMenu (Member member, MenuCreateRequest request){
        Menu addData = new Menu();
        addData.setMember(member);
        addData.setTotalPrice(request.getTotalPrice());
        addData.setHellDate(request.getHellDate());
        addData.setMenuEtc(request.getMenuEtc());

        menuRepository.save(addData);
    }

    public List<MenuItem> getMenus(){
        List<Menu> originList = menuRepository.findAll();

        List<MenuItem> result = new LinkedList<>();
        for (Menu menu : originList){
            MenuItem addItem = new MenuItem();
            addItem.setId(menu.getId());
            addItem.setMemberName(menu.getMember().getName());
            addItem.setTotalPrice(menu.getTotalPrice());
            addItem.setHellDate(menu.getHellDate());

            result.add(addItem);
        }
        return result;
    }

    public MenuResponse getMenu(long id){
        Menu originData = menuRepository.findById(id).orElseThrow();

        MenuResponse response = new MenuResponse();
        response.setId(originData.getId());
        response.setMemberName(originData.getMember().getName());
        response.setTotalPrice(originData.getTotalPrice());
        response.setHellDate(originData.getHellDate());
        response.setMenuEtc(originData.getMenuEtc());

        return response;
    }

    public void putMenu(long id, MenuChangeRequest request){
        Menu originData = menuRepository.findById(id).orElseThrow();

        originData.setTotalPrice(request.getTotalPrice());
        originData.setHellDate(request.getHellDate());
        originData.setMenuEtc(request.getMenuEtc());

        menuRepository.save(originData);
    }

    public void delMenu(long id){
        menuRepository.deleteById(id);
    }
}
