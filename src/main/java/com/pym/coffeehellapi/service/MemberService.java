package com.pym.coffeehellapi.service;

import com.pym.coffeehellapi.entity.Member;
import com.pym.coffeehellapi.model.member.MemberCreateRequest;
import com.pym.coffeehellapi.model.member.MemberInfoChangeRequest;
import com.pym.coffeehellapi.model.member.MemberItem;
import com.pym.coffeehellapi.model.member.MemberResponse;
import com.pym.coffeehellapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id){
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request){
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers(){
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        for (Member member : originList){
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setName(member.getName());
            addItem.setPhoneNumber(member.getPhoneNumber());

            result.add(addItem);
        }
        return result;
    }

    public MemberResponse getMember(long id){
        Member originData = memberRepository.findById(id).orElseThrow();

        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }

    public void putMemberInfo(long id, MemberInfoChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();

        originData.setName(request.getName());
        originData.setPhoneNumber(request.getPhoneNumber());
        originData.setEtcMemo(request.getEtcMemo());

        memberRepository.save(originData);
    }
}
